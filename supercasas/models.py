from django.db import models

# Create your models here.
class TipoCasa(models.Model):
    nombre =models.CharField(max_length=30)
    def __str__ (self):
        return self.nombre

class Casa(models.Model):
    nombre =models.CharField(max_length=30)
    metro =models.IntegerField()
    baños =models.IntegerField()
    habitaciones =models.IntegerField()
    valor =models.IntegerField(null=True)
    descripcion =models.CharField(max_length=300)
    tipocasa =models.ForeignKey(TipoCasa, on_delete=models.PROTECT )
    terraza =models.BooleanField()
    imagen =models.ImageField(upload_to="casas",null=True)   
    

    def __str__ (self):
        return self.nombre

class Contacto(models.Model):
    nombre = models.CharField(max_length=30)
    correo = models.EmailField()
    numero = models.IntegerField()
    mensaje = models.CharField(max_length=300)
    Subcribirse = models.BooleanField()
    def __str__ (self):
        return self.nombre          



