from django.shortcuts import render
from .forms import ContactoForm
from .models import Casa
# Create your views here.
def home(request):
    return render(request, 'supercasas/home.html',{}) 
def nosotros(request):
    return render(request, 'supercasas/nosotros.html',{})
def casaamedida(request):
    casas = Casa.objects.all()
    data = {
        'CasasD': casas
    
    }
    return render(request, 'supercasas/casa_medida.html', data)
def casaametro(request):    
    
    return render(request, 'supercasas/casa_por_metro.html', {})    
def galeria(request):
    return render(request, 'supercasas/galeria.html',{})
def contacto(request):
    data = {
        'form': ContactoForm()
    }
    if request.method == 'POST' :
        formulario = ContactoForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            data["mensaje"] = "Contacto guardado, Nos Comunicaremos contigo de forma telefonica o via email"
        else:
            data["form"] = formulario       
    return render(request, 'supercasas/contacto.html',data )