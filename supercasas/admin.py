from django.contrib import admin
from .models import TipoCasa, Casa , Contacto
# Register your models here.
admin.site.register(TipoCasa)
admin.site.register(Casa)
admin.site.register(Contacto)
