from django.urls import path
from .views import home, nosotros, galeria, contacto ,casaamedida , casaametro 

urlpatterns = [
    path('', home, name="home"),
    path('nosotros/', nosotros, name="nosotros"),
    path('contacto/', contacto, name="contacto"),
    path('galeria/', galeria, name="galeria"),
    path('casa-a-medida/', casaamedida, name="casaamedida"),
    path('casa-por-metro/', casaametro, name="casaametro"),
]   