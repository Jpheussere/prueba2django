from django.apps import AppConfig


class SupercasasConfig(AppConfig):
    name = 'supercasas'
